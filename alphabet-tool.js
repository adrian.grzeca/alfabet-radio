// Consts
const WORDBANKS = {
    PLCALLSIGNS: "words-plcallsigns",
    PL: "words-pl",
    EN: "words-en"
}

const ALPHABETS = {
    ITU: "alfa-itu",
    PL: "alfa-pl"
}

const WORDBANK_BUTTONS = [
    document.getElementById(WORDBANKS.PLCALLSIGNS),
    document.getElementById(WORDBANKS.PL),
    document.getElementById(WORDBANKS.EN)
];

const ALPHABET_BUTTONS = [
    document.getElementById(ALPHABETS.ITU),
    document.getElementById(ALPHABETS.PL)
];

// Functions
function selectButtonWordBank(wordbank) {
    for (let i = 0; i < WORDBANK_BUTTONS.length; i++) {
        if (wordbank != WORDBANK_BUTTONS[i]) {
            WORDBANK_BUTTONS[i].classList.remove("btn-primary");
            WORDBANK_BUTTONS[i].classList.add("btn-outline-primary");
        }
        else {
            WORDBANK_BUTTONS[i].classList.remove("btn-outline-primary");
            WORDBANK_BUTTONS[i].classList.add("btn-primary");
            selectedWordBank = WORDBANK_BUTTONS[i].id
        }
    }
    console.log("Selected word bank is " + selectedWordBank);
}

function selectButtonAlphabet(alphabet) {
    for (let i = 0; i < ALPHABET_BUTTONS.length; i++) {
        if (alphabet != ALPHABET_BUTTONS[i]) {
            ALPHABET_BUTTONS[i].classList.remove("btn-primary");
            ALPHABET_BUTTONS[i].classList.add("btn-outline-primary");
        }
        else {
            ALPHABET_BUTTONS[i].classList.remove("btn-outline-primary");
            ALPHABET_BUTTONS[i].classList.add("btn-primary");
            selectedAlphabet = ALPHABET_BUTTONS[i].id
        }
    }
    console.log("Selected alphabet is " + selectedAlphabet);
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}


function generatePLCallsign() {
    const PREFIXES = ["HF", "SN", "SO", "SP", "SQ", "3Z"];
    const CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    const CHARSANDNUMBERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"

    let callsign = PREFIXES[getRandomInt(0, PREFIXES.length)];
    callsign += getRandomInt(0, 9).toString();
    const LEN = getRandomInt(1, 4);
    for (let i = 0; i < LEN - 1; i++) {
        callsign += CHARSANDNUMBERS[getRandomInt(0, CHARSANDNUMBERS.length)];
    }
    callsign += CHARS[getRandomInt(0, CHARS.length)];

    return callsign;
}

function generatePL() {
    const list = ["wielkosc", "ziemianka", "akcent irlandzki", "ruten", "kumpel", "slusznosc", "wojna", "lekkosc", "astrologia", "kwiat", "powtorzenie", "bogaty", "lecytyna", "exodus", "metody", "depolaryzacja", "regurgitacja", "fonogram", "omdlenie", "emu", "orogeneza", "prawidlowosc", "karfizm", "strach", "sprzeniewierzenie", "przechwalac", "serwetka", "nigdzie", "spokoj", "tepota", "rho", "eskadra", "klasyfikator", "shea", "recydywista", "defetystyczny", "gestapo", "hartland", "nieuprzejmosc", "ochryply", "tackler", "ucho", "zrebie", "decydujac", "komutacja", "dupleks", "cobb", "podawac", "polowanie", "trakcja", "kanister", "obsypnik", "fascynowac", "dywagowac", "denaturalizowac", "bohemizowac", "pomnikalnosc", "ziemniak", "sennosc", "dwochsetletni", "poliowirus", "boula", "uczen", "osad", "glinowac", "obgartywac", "odcyfrowac", "odmagnesowywac", "poczestowac", "rozromansowac", "skanalizowac", "skandalizowac", "szwarcowac", "wcalowywac", "jurajski", "fleksyjny", "cienisty", "nogi", "znitowac", "zetatyzowac", "zdegustowac", "zasumowac", "zagruzowa", "zaatakowac", "wymyszkowac", "werblowac", "mojzeszowy", "nienawistny", "nieracjonalny", "odsetkowy", "petrochemiczny", "pelnoprawny", "pobratymczy", "rurowy", "szatanski", "zarozumialy", "zawadiacki", "zdroworozsadkowy", "podstepny", "zorientowany"];
    return list[getRandomInt(0, list.length)];
}

function generateEN() {
    const list = ["doppelgangers","interclass","north","hairsplitter","thickeners","dichotomous","graveyards","depilator","resignednesses","ultrafiltrates","gutters","kicks","entirety","intercom","numismatic","tenorist","alder","conveyance","radicalization","far","rejudging","abscond","recontours","supernumeraries","subcell","abuser","crosswise","overlighted","skulked","gadrooning","firepot","zooglea","insaner","breechblock","blob","mismatches","cicero","uncovering","ariose","mulcting","teth","oompah","lanner","yah","clutchy","licenser","talcing","laitances","supernormally","aiguille","pedipalp","mimeograph","chords","glister","hijacks","kytes","jerid","nailbiter","underfeeding","masculinise","tangential","hypospadias","jarovized","pearliest","scribed","flyswatters","bandy","rhinoceroses","thyrse","evinces","craving","faggotries","patentable","nun","grunges","tweezer","replenishments","handwrote","hallal","hereunto","teach","crushable","airmails","kernelly","hyphenation","twice","firrier","philologists","puree","fancifulness","anageneses","foilsman","galliums","carpetbaggery","monocle","app","backlash","inveighing","hider","outspreads"];
    return list[getRandomInt(0, list.length)];
}

function generateRandomWord() {
    switch (selectedWordBank) {
        case WORDBANKS.PLCALLSIGNS:
            currentWord = generatePLCallsign();
            break;
        case WORDBANKS.PL:
            currentWord = generatePL();
            break;
        case WORDBANKS.EN:
            currentWord = generateEN();
            break;
    }
    console.log("Random word is " + currentWord)
    document.getElementById("output").innerText = currentWord;
}

function updateScore() {
    document.getElementById("score").innerText = successful + "/" + total;
}

function Letter(validSpelling, possibleSpelling) {
    this.validSpelling = validSpelling;
    this.possibleSpelling = possibleSpelling;
    this.check = function (input) {
        if (possibleSpelling.includes(input)) return true;
        else false;
    };
}

const ITUDICTONARY = {
    "a": new Letter("alfa", ["alfa"]),
    "b": new Letter("bravo", ["bravo"]),
    "c": new Letter("charlie", ["charlie"]),
    "d": new Letter("delta", ["delta"]),
    "e": new Letter("echo", ["echo"]),
    "f": new Letter("foxtrot", ["foxtrot"]),
    "g": new Letter("golf", ["golf"]),
    "h": new Letter("hotel", ["hotel"]),
    "i": new Letter("india", ["india"]),
    "j": new Letter("juliet", ["juliet", "jultiet"]),
    "k": new Letter("kilo", ["kilo"]),
    "l": new Letter("lima", ["lima"]),
    "m": new Letter("mike", ["mike"]),
    "n": new Letter("november", ["november"]),
    "o": new Letter("oscar", ["oscar"]),
    "p": new Letter("papa", ["papa", "jp2gmd"]),
    "q": new Letter("quebec", ["quebec", "kłebek", "kebek", "kebab"]),
    "r": new Letter("romeo", ["romeo"]),
    "s": new Letter("sierra", ["sierra"]),
    "t": new Letter("tango", ["tango"]),
    "u": new Letter("uniform", ["uniform"]),
    "v": new Letter("victor", ["victor", "wiktor"]),
    "w": new Letter("whisky", ["whisky"]),
    "x": new Letter("x-ray", ["x-ray", "xray", "x-rey", "xrey"]),
    "y": new Letter("yankee", ["yankee", "yanke", "janki", "jankee", "janke"]),
    "z": new Letter("zulu", ["zulu"]),
    "0": new Letter("zero", ["zero"]),
    "1": new Letter("one", ["one"]),
    "2": new Letter("two", ["two"]),
    "3": new Letter("three", ["three"]),
    "4": new Letter("four", ["four"]),
    "5": new Letter("five", ["five"]),
    "6": new Letter("six", ["six"]),
    "7": new Letter("seven", ["seven"]),
    "8": new Letter("eight", ["eight"]),
    "9": new Letter("nine", ["nine", "niner"])
}

const PLDICTONARY = {
    "a": new Letter("adam", ["adam"]),
    "b": new Letter("barbara", ["barbara"]),
    "c": new Letter("cezary", ["cezary"]),
    "d": new Letter("dorota", ["dorota"]),
    "e": new Letter("ewa", ["ewa"]),
    "f": new Letter("franciszek", ["franciszek"]),
    "g": new Letter("grażyna", ["grażyna", "grazyna"]),
    "h": new Letter("henryk", ["henryk"]),
    "i": new Letter("irena", ["irena"]),
    "j": new Letter("jadwiga", ["jadwiga"]),
    "k": new Letter("karol", ["karol"]),
    "l": new Letter("ludwik", ["ludwik"]),
    "m": new Letter("maria", ["maria"]),
    "n": new Letter("natalia", ["natalia"]),
    "o": new Letter("olga", ["olga"]),
    "p": new Letter("paweł", ["paweł", "pawel"]),
    "q": new Letter("quebec", ["quebec", "kłebek", "kebek", "kebab"]),
    "r": new Letter("roman", ["roman"]),
    "s": new Letter("stanisław", ["stanisław", "stanislaw"]),
    "t": new Letter("tadeusz", ["tadeusz"]),
    "u": new Letter("urszula", ["urszula"]),
    "v": new Letter("violetta", ["violetta", "violeta", "wioletta", "wioleta"]),
    "w": new Letter("wanda", ["wanda"]),
    "x": new Letter("xsawery", ["xsawery", "ksawery"]),
    "y": new Letter("ypsylon", ["ypsylon", "ypsilon", "ipsilon", "ipsylon"]),
    "z": new Letter("zygmunt", ["zygmunt"]),
    "0": new Letter("zero", ["zero"]),
    "1": new Letter("jeden", ["jeden"]),
    "2": new Letter("dwa", ["dwa"]),
    "3": new Letter("trzy", ["trzy"]),
    "4": new Letter("cztery", ["cztery"]),
    "5": new Letter("pięć", ["pięć", "piec", "pięc", "pieć", "pienc", "pienć"]),
    "6": new Letter("sześć", ["sześć", "szesc", "sześc", "szesć"]),
    "7": new Letter("siedem", ["siedem"]),
    "8": new Letter("osiem", ["osiem"]),
    "9": new Letter("dziewieć", ["dziewieć", "dziewiec"])
}

function checkITU() {

    return valid;
}

function newWord() {
    total++;
    updateScore();
    generateRandomWord();
}

function check() {
    // Init variables
    let valid = true;
    let dict = ITUDICTONARY;
    let validOutput = "";

    // Get input
    const input = document.getElementById("input").value.trim().toLowerCase().split(" ");
    const validElm = document.getElementById("valid");
    const last = document.getElementById("last");

    // Select Alphabet
    switch (selectedAlphabet) {
        case ALPHABETS.ITU:
            dict = ITUDICTONARY;
            break;
        case ALPHABETS.PL:
            dict = PLDICTONARY;
            break;
    }

    // Check word length
    if (input.length != currentWord.length) {
        validElm.innerHTML = "Nie prawidłowa ilość wyrazów! Oddzielaj je spacją!"
        valid = false;
    }
    else {
        // Check each word
        for (let i = 0; i < input.length; i++) {
            const letter = currentWord[i].toLowerCase();
            const word = input[i];
            
            // STUPIDLY CHECK
            if(word == "pedal"){
                alert("DEBILU PAWEŁ A NIE PEDAŁ!!!!!!!!!!!!!!!!!!!");
            }

            // Check
            if (dict[letter].check(word)) {
                validOutput += dict[letter].validSpelling;
            }
            else {
                validOutput += "<b>" + dict[letter].validSpelling.toUpperCase() + "</b>";
                valid = false;
            }
            if (i < input.length - 1) validOutput += " ";
        }

        validElm.innerHTML = validOutput;
    }



    if (valid) {
        successful++;
        last.innerText = "DOBRZE!";
        last.classList.remove("text-danger");
        last.classList.add("text-success");
    }
    else {
        last.innerText = "ŹLE!";
        last.classList.add("text-danger");
        last.classList.remove("text-success");
    }
    newWord();
    document.getElementById("input").value = "";
}

// Assign initial things
let selectedWordBank = 0;
let selectedAlphabet = 0;
let successful = 0;
let total = 0;
let currentWord = "";
selectButtonWordBank(WORDBANK_BUTTONS[0]);
selectButtonAlphabet(ALPHABET_BUTTONS[0]);
generateRandomWord();
updateScore();

// Enter listener
input.addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();
        check();
    }
});